# 运行时环境

#### Description
它有别于通篇使用英语字母字符，开始使用汉字字符计算机代码，平台支持鸿蒙操作系统文件格式，功能是一款运行时环境软件，由开发者河南省封丘县和谐路301号谢非凡建仓编写，唯一代码仓库被代码托管在广东省福田区中康路136号Gitee平台。

#### 技术愿景
希望运行时环境软件能够带给学习英语语言困难而且生活激昂奋进的人们一项发展道路分支，你只需要熟悉汉语母语，就能和英语擅长者相同水准的使用汉字编写计算机代码，成为一名合格的开发者。

#### 应用领域
1、运行时环境软件使得人们在智能手机开发编程，我一直都单方面旨在使该运行时环境软件和Jetbrains公司的Kotlin编程语言兼容并包。
2、我一直都在坚定支持华为鸿蒙操作系统的蓬勃发展，并且自豪作为华为开发者联盟一员。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
